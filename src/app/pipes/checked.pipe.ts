import { Pipe, PipeTransform } from '@angular/core';
import {Product} from '../models/models';

@Pipe({
  name: 'checked'
})
export class CheckedPipe implements PipeTransform {
  transform(values: Array<Product>): any {
    console.log('asd');
    return values.filter((item) => item.isPromoted);
  }

}
