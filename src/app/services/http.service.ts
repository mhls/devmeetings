import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {retry} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient) {
  }

  getProducts() {
    return this.httpClient.get('https://shining-torch-4509.firebaseio.com/products.json')
      .pipe(retry(5));
  }
}
