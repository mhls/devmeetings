import { Injectable } from '@angular/core';
import {Product} from '../models/models';
import {HttpService} from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsRepositoryService {
  products: Product[] = [
  ];
  constructor(private httpService: HttpService) {
    this.httpService.getProducts().subscribe((objects) => {
      Object.keys(objects).forEach((key) => {
        objects[key].description = key;
        this.products.push(objects[key]);
      });
    });
  }
}
