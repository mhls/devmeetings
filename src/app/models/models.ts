export interface Product {
  name: string;
  description: string;
  price: number;
  isPromoted: boolean;
  tags: Array<string>;
  imageUrl: string;
}
