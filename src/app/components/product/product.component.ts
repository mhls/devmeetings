import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent {
  @Input() public product;
  @Input() public filtered = false;
  @Output() public productChanged = new EventEmitter();

  productChangedCall() {
    this.productChanged.emit(this.product);
  }
}
