import { Component, Input } from '@angular/core';
import {Product} from '../../models/models';
import {FormControl} from '@angular/forms';
import {ProductsRepositoryService} from '../../services/products-repository.service';
import {HttpService} from '../../services/http.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {
  products: Product[];
  filteredProductsByTag: Product[];
  filteredProducts: Product[];
  promotedProducts: Product[];
  sortTranslate = {
    asc: 'malejaco',
    desc: 'rosnaco'
  };
  sort = 'asc';
  searchInput = new FormControl();

  constructor(private productsService: ProductsRepositoryService, private httpService: HttpService ) {
    this.httpService.getProducts();
    this.products = this.productsService.products;
    this.filteredProductsByTag = this.products;
    this.filteredProducts = this.products;
    this.promotedProducts = this.products.filter((item) => item.isPromoted);
    this.sortByPrice();
    this.searchInput.valueChanges.subscribe(this.filter.bind(this));
  }

  filterByTag(evt) {
    if (evt !== '') {
      this.filteredProductsByTag = this.products.filter((item) => (item.tags.indexOf(evt) !== -1));
    } else {
      this.filteredProductsByTag = this.products;
    }
  }

  filter(value) {
    if (value !== '') {
      this.filteredProducts = this.products.filter((item) => {
        console.log(item);
        return ((item.name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.description.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.price === value) ||
        (item.tags.indexOf(value.toLowerCase()) !== -1));
      });
    } else {
      this.filteredProducts = this.products;
    }
  }

  productChanged() {
    this.promotedProducts = this.products.filter((item) => item.isPromoted);
  }

  sortByPrice() {
    this.sort = (this.sort === 'asc') ? 'desc' : 'asc';
    this.products.sort(this.sortFn.bind(this));
  }

  sortFn(a, b) {
    return (this.sort === 'asc') ? a.price - b.price : b.price - a.price;
  }
}
